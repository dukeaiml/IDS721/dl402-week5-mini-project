# IDS 721 Mini Project 5
This project is to Create a Rust AWS Lambda function and implement a simple service which connected to the database. This project can take a json input and post it into the database.

## Packages and Dependencies
The project uses AWS DynamoDB as backend database. To create a DynamoDB, you can simply go to the AWS cloud console and search for DynamoDB and create one. 

Add the permissions to the IAM role you used. The permissions include `AmazonDynamoDBFullAccess`, `AWSLambda_FullAccess`, and `AWSLambdaBasicExecutionRole`. This will ensure that the lambda function and the read/write for DynamoDB works.

## Build and deploy the project:
- Run `cargo new week5` to create a blank project.
- Update the code in `/src/main.rs` file to add functionalities to the rust function, where the users can `put_item` into the DynamoDB. Remeber to use the same database name you created in the cloud console for the code.  
- Add dependencies to `Cargo.toml` file.
- Run `cargo build --release` to build the application
- Run `cargo build deploy --region <region> --iam-role <IAM ROLE>` to deploy the rust lambda function you wrote. 
- After the lambda function is deployed to AWS cloud, you can go to the console and add a API Gateway for the corresponding lambda function. 
- Go to test console and input the desired format for you json input and that item will be added to the DynamoDB database.

## Screenshots
- Screenshot of IAM role configuration: 
![Alt text](/screenshots/IAM.png)
- Screenshot of lambda function and API Gateway:
![Alt text](/screenshots/lambda.png)
- Screenshot a test input that is successful:
![Alt text](/screenshots/test.png)
- Screenshot of DynamoDB after the data is added:
![Alt text](/screenshots/Dynamo.png)

